import React from 'react'
import { Route, Switch } from 'react-router-native'
import { ConnectedRouter } from 'connected-react-router'
import { Provider } from 'react-redux'
import { store, history, persistor } from './AppStore.js'

import App from './App.js'
import PageUser from './PageUser.js'
import PageEditItem from './PageEditItem.js'
import PageAddItem from './PageAddItem.js'

export default class Router extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <PersistGate 
                loading={null} 
                persistor={persistor}>
                    <ConnectedRouter history={history}>
                        <Switch>
                            <Route exact path={'/'} component={App} />
                            <Route exact path={'/user/edit'} component={PageUser} />
                            <Route exact path={'/item/add'} component={PageAddItem} />
                            <Route exact path={'/item/edit'} component={PageEditItem} />
                        </Switch>
                    </ConnectedRouter>
                </PersistGate>
            </Provider>)
    }
}