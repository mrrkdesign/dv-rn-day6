import { createMemoryHistory } from 'history'
import { applyMiddleware, createStore, combineReducers, compose } from 'redux'
import { routerMiddleware, connectRouter } from 'connected-react-router'
import logger from 'redux-logger'

import { userReducer, itemsReducer } from './AppReducer'

const reducers = (history) => combineReducers({
    items: itemsReducer,
    user: userReducer,
    router: connectRouter(history)
})

const persistConfig = {
    key: 'root',
    storage,
  }
  
const persistedReducer = persistReducer(persistConfig, reducers(history))
export const history = createMemoryHistory()
export const store = createStore(
    persistedReducer, 
    compose(applyMiddleware(routerMiddleware(history),logger))
)
export const persistor = persistStore(store)