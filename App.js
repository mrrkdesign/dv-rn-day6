import React from 'react'
import { 
  StyleSheet, 
  FlatList,
  TextInput, 
  Modal,
  View, 
  Text, 
  TouchableOpacity 
} from 'react-native';
import { connect } from 'react-redux';
import { push } from 'connected-react-router'

class App extends React.Component {
  state = {
    editItem: null,
  }
  goToEditUser = () => {
    const { push } = this.props
    push('/user/edit')
  }
  goToAddItem = () => {
    const { push } = this.props
    push('/item/add')
  }
  goToEditItem = (index) => {
    const { push } = this.props
    push('/item/edit', { index })
  }
  renderItem = ({item, index}) => (
    <TouchableOpacity key={index} onPress={() => this.goToEditItem(index)}>
      <Text>{item.name}</Text>
    </TouchableOpacity>)
  render(){
    const { user, items } = this.props
    const { editItem, isShowUserModal, isShowAddItemModal, isShowEditItemModal } = this.state
    return (
      <View style={styles.container}>
        <View>
          <View>
          <Text>Username: {user.username}</Text>
          <Text>Password: {user.password}</Text>
          <TouchableOpacity onPress={() => this.goToEditUser()}>
            <Text>Edit User</Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity onPress={() => this.goToAddItem()}>
            <Text>Add Item</Text>
          </TouchableOpacity>
        <FlatList 
          data={items}
          renderItem={this.renderItem}
         />
      </View>
    </View>)
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  
})
export default connect(state => state, { push })(App)