import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
export const userReducer = (state = {
    username: 'test',
    password: 'eiei'
}, action) => {
    switch(action.type){
        case 'SAVE_USER': return {...state, ...action.payload}
        default:
            return state;
    }
}
export const itemsReducer = (state = {}, action) => {
    switch(action.type){
        case 'ADD_ITEM': 
            return state
                .map(item => item)
                .push(action.payload)
        case 'EDIT_ITEM': 
            return state
                .map((item, index) => 
                    index === action.payload.index 
                    ? {...item, ...action.payload} : item)
        case 'REMOVE_ITEM': 
            return state
                .filter(item => index !== action.payload.index)
        default:
            return state;
    }
}

// export default combineReducers({ 
//     user: userReducer, 
//     items: itemsReducer
// })

// export default 