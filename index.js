/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React from 'react';
import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';
import Router from './Router';
AppRegistry.registerComponent(appName, () => Router);
