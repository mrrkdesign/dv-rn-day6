import React from 'react'
import { connect } from 'react-redux'
import { TextInput, TouchableOpacity, StyleSheet, Modal, View, Text } from 'react-native'
import { saveUser } from './Actions'
import { goBack } from 'connected-react-router'
class PageAddItem extends React.Component {
    state = {
        username: '',
        password: ''
    }
    componentDidMount(){
        const { username , password } = this.props
        this.setState({
            username,
            password
        });
    }
    onChangeValue = (index, value) => this.setState({ [index]: value })
    render(){
        const { username, password, onSave, onCancel } = this.props
        return (
        <View style={styles.container}>
            <TextInput value={username} onChangeText={(value) => onChangeValue('username', value)} />
            <TextInput value={password} onChangeText={(value) => onChangeValue('password', value)} />
            <View style={[styles.container, { flexDirection: 'row' }]}>
                <TouchableOpacity style={styles.container} onPress={onSave}>
                    <Text>Save</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.container} onPress={onCancel}>
                    <Text>Cancel</Text>
                </TouchableOpacity>
            </View>
        </View>)
    }
}

const styles = StyleSheet.create({
    container: { flex: 1 }
})

export default connect(
    state => state.user, 
    dispatch => ({
        onSave: (state) => dispatch(saveUser(state)),
        onCancel: () => dispatch(goBack())
    })
)(PageAddItem)