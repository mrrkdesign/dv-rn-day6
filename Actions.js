export const saveUser = (state) => ({ 
    type: 'SAVE_USER', 
    payload: state
})
export const addItem = (state) => ({ 
    type: 'ADD_ITEM', 
    payload: state
})
export const editItem = (index, state) => ({ 
    type: 'EDIT_ITEM', 
    payload: {index, ...state}
})
export const removeItem = (index) => ({ 
    type: 'REMOVE_ITEM',
    paylaod: { index }
})